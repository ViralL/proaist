'use strict';

// ready
$(document).ready(function() {

    // anchor
    // $(".anchor").on("click","a", function (event) {
    //     event.preventDefault();
    //     var id  = $(this).attr('href'),
    //         top = $(id).offset().top;
    //     $('body,html').animate({scrollTop: top + 40}, 1000);
    // });
    // anchor

    // search
    $('.search--js').click(function () {
        $(this).prev().slideToggle();
        $(this).parents().find('body').addClass('shadow');
        return false;
    });
    $('.close--js').click(function () {
        $(this).closest('.search--body').hide();
        $(this).parents().find('body').removeClass('shadow');
        return false;
    });
    // search

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    //$('.slider').slick({});
    // slider

    $('.test__item').each( function() {
        $('input').on('change', function() {
            $(this).parent().parent().next().show();
        });
    });

    // select {select2}
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // popup {magnific-popup}
    //$('.image-gallery').magnificPopup({});
    // popup


    //.slider-line--js
    $('.slider-line--js').each(function(){
        var per = $(this).attr('data-per');
        $(this).css('width', per + '%');
        if ( per > 80 ) {
            $(this).css('background-color','#ffab6e');
        } else if ( per > 60 && per <= 80) {
            $(this).css('background-color','#ffd7bb');
        } else if ( per > 40 && per <= 60) {
            $(this).css('background-color','#76d8e4');
        } else {
            $(this).css('background-color','#b5e6e5');
        }
    });
    //.slider-line--js


    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs

});
// ready


// load
$(document).load(function() {});
// load

// scroll
$(window).scroll(function() {});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts